# pheidippides

Beginnings of a messaging framework for clojure.

## Why yet another messaging framework?

One may rightly question why I dont just use Aleph, or perhaps onyx or even just write some
stuff on top of Netty.

This is mostly a learning project, as I the author wanted to learn the java NIO capabilities
as well as learn SSLEngine.  Since messaging frameworks are becoming quite popular, it's a
good idea to understand conceptually how they work.  And as Richard Feynman once said,

    "What I can not create, I do not understand"

Pheidippides will keep an eye on performance, and as such will be a binary messaging protocol.
It will not send json though it could receive as general data, a json encoded message.
However, the messages that it sends and receives are themselves binary.


## What's up with the name?

Pheidippides was the Greek courier at marathon who informed the Greeks of their victory over
the Persians.

## Use cases

The initial use case for pheidippides was as a replacement for making SSH calls in an existing
test framework.  As I thought about it, I realized it would be nice to have a fast and compact
protocol for doing RPC.  However, many RPC mechanisms have drawbacks

- In http REST style, there's all the overhead of sending http
  - including XML-RPC, JSON-RPC and SOAP
  - JSON is too restrictive in sending of types
- Most RPC mechanisms are synchronous and blocking (request/response)
- Websockets solved the problem of asynchronous communication between server and client
  - But still required the author writing a message protocol anyway

So pheidippides was designed to be asynchronous and reasonably fast.  This allowed some additional
use cases:

- Events can be sent to a topic that services can listen to asynchronously
  - in other words, Service A might be minding it's own business and receive event from Service B
- Services implement an interface and more than one Service can implement the same interface
- The transporting of messages should be abstracted to work with more than just TCP/IP

So in a nutshell, pheidippides can be used when you need services that can talk to each other
with more than a simple request/response style.  For example, chat applications, game servers,
or any other application that requires responses to events in (semi) real time.

## Usage

pheidippides is in too early of a stage to be useful

## License

Copyright © 2015

Distributed under the Apache License Version 2.0
