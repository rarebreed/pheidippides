# Introduction to pheidippides

Pheidippides is a general purpose messaging service which combines a controller registry along with a basis to write
services.  Pheidippides also implements its own protocol designed for asynchronous and event driven messages

Pheidippides provides:

- A non-blocking asynchronous Controller which provides a registry of available services
- A protocol for services to talk to each other through the controller
- A clojure implementation of a non-blocking service


# Requirements:  TODO

Describe the major things that need to be done to get pheidippides working

## Route Table

Currently, the Controller accepts a Module's connections (one for regular message, and a second just
for Events).  However, the Controller does not maintain a mapping of these connections.  This map
will be necessary for Messages from one Module to be sent to another.

1. The route table must be shared

**Shared Route Table**
In order to handle failover events, the route table must be shared.  This is so that if one Controller
goes down, another Controller can examine the shared route table and pick up any routing.


## Registry

The registry is a manifest of all the connected modules, and what services the module exposes.  Where
the Route table maintains connection information of the Modules, the Registry contains information
about what the Modules can do.

1. The registry must be shared

