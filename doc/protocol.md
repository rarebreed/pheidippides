# Message protocol pheidippides

This document will informally describe the protocol that pheidippides uses.  The protocol will be
broken down into 2 main categories:

- Transport Protocol
- Service Protocol

## Definitions

Here, we will describe some terms that have specific meaning within the context of pheidippides.

**Message**:
A message is the encapsulation of data.  All Modules talk to each other through Messages, and Messages get routed to
each other through a Controller.  Messages have various types such as service-request, register, or send-data

**Interface**:
An interface is a declaration of methods or functionality that a Module can provide.

**Module**:
A Module can be a provider of an Interface, a consumer of an Interface, a sender of Events or a watcher of Events

**Events**:
A Module can produce Events which basically are just a packet of information that other Modules might be interested in.
Conversely, a Module can register interest in a source of Events.  Although Events are just a type of Message, they
are sent via another

**Topic**:
Modules send Events to a Topic.  Modules can publish Events to a Topic, or a Module can subscribe to a Topic in order
to receive them.

**Transport**:
Modules ultimately do the sending and receiving of messages through a Transport.  The Endpoint is an abstraction so that
we can have a single API whether the underlying transport is TCP/IP, dbus, or native.

**Channel**:
An abstraction of a communication pipeline.  It has a Transport to know how to physically transport a message.  In
pheidippides, there are 2 main Channels: one for sending general purpose messages, and another for sending out of band
events.


## Overview

pheidippides is all about sending Messages.  Messages follow a specified format and based on the contents of the
Message, when a Module receives a Message, if the Message is valid, the Module will attempt to act on the Message.  In
order to decouple Modules from each other, Modules only directly talk to a Controller.  The Controller maintains a
Registry of all the other Modules and the Interfaces they provide.  When a Message is sent, it sends as part of the
Message the :source-id (which is the sender of the Message), and a :destination-id (which is the name of an Interface).
The controller will look to see if the Interface exists.  Since multiple Modules might be providing the same Interface,
the requesting Module can optionally provide a concrete implementation (if the concrete type does not exist, it is
ignored).

The actual low-level transporting of messages is handled through an abstraction called a Transport.  A Transport can
either be :tcpip (which uses SocketChannels) or :native (which uses core.async channels).  Eventual dbus support is
being looked into once :tcpip and :native are implemented.  It is possible that Module A is using a :tcpip Transport
and Module B is using a :native Transport, but A and B should still be able to communicate with each other via the
Controller (the Controller will know about all Transport implementations, and will be able to bridge the low-level
transporting of Messages from one implementation of a Transport to another).

pheidippides uses 2 Channels to send Messages to other Modules.  One Channel is used for any of the Message types other
than get-event and send-event.  This is done because if a Module is receiving a large message (or is on a slow
connection) the SocketChannel can be starved.  Perhaps you are thinking that since this is non-blocking, why not just
use a Selector to choose between multiple incoming SocketChannels?  Recall that Modules only talk to the Controller.
Since all communication is routed through the Controller, there is only one socket normally.  Therefore, there are not
multiple incoming SocketChannels to select from.

By having each Module have 2 Channels, the Controller maintains a mapping of 2 Channels per module, one for regular
Message types, and another for Event Message types.  When the controller sees from the opcode that a Message is of an
Event Message type, it will forward the message to the destination Module via the SocketChannel mapped for Event
handling.  In some ways, you can think of this separate channel like an IRQ line.  While a Module is getting the byte
stream for a Message on its Regular Channel, it can also get it from the Event channel.

Currently, there are two threads monitoring the Regular and Event Channels, but eventually, this code will move to use
a Selector, using the message type to for the SelectionKey.

## Message Types

Just as http has GET, POST and other message types, pheidippides also declares several message types.

| Message Type          | Description
|-----------------------|-----------------------------
| register              | registers the Module with a Controller
| interface-list        | lists all the interfaces that have been registered
| call                  | calls a method from an interface
| interface-reflect     | lists the functions in an interface
| send-data             | used when bulk transferring data (eg, file contents)
| get-data              | used when retrieving bulk data (eg, file contents)
| send-event            | sends data to a Topic
| get-event             | gets data from a Topic
| subscribe-topic       | adds source-id to a Topic (source-id will receive events from this Topic)
| publish-topic         | creates a Topic that a Module can send Events to
| unregister            | unregisters a Module from the Controller


### register

Modules can't send Messages until it has registered itself with the Controller.  If the module is already registered
and registers again, then the Controller will update its registry table with the latest request.

**Request**

| Field       |  Value                | Notes                       |
|-------------|-----------------------|-----------------------------|
| opcode      | 0x00                  |                             |
| source-id   | domain:module-name    |(eg "com.zinger.qe:testMe")  |
| dest-id     | []                    | empty vector                |
| msg-tag     | nil                   |                             |
| resp-type   | 0x00                  | ACK                         |
| service-tag | nil                   |                             |
| data        | {}                    | see below                   |

```
    {:interfaces {:interface-name }
    }
```

**Response**

ACK Message type if successful, NACK if unsuccessfully registered

**Back end**

The controller will persist registry information to a database.

### unregister

Unregisters a Module from the Controller.

### interface-list

A Module can request from the Controller a list of all the interfaces provided by other Modules that have registered
to the Controller.

*Request*

| Field       |  Value                | Notes                       |
|-------------|-----------------------|-----------------------------|
| opcode      | 0x01                  |                             |
| source-id   | domain:module-name    |(eg "com.zinger.qe:testMe")  |
| dest-id     | []                    | empty vector                |
| msg-tag     | nil                   |                             |
| resp-type   | 0x01                  | RESP                        |
| service-tag | nil                   |                             |
| data        | {:module-name}        | name of module to list      |

*Response*

| Field       |  Value                | Notes                       |
|-------------|-----------------------|-----------------------------|
| opcode      | 0x01                  |                             |
| source-id   | controller:path       | eg controller:host-name     |
| dest-id     | []                    | empty vector                |
| msg-tag     | 0x00                  |                             |
| resp-type   | 0x00                  | ACK                         |
| service-tag | nil                   |                             |
| data        | {}                    | see below                   |

*data*

Is a vector of ModuleFunction type

```
    [{:name "foo"
      :args [{:name "x" :type nil} {:name "y" :type :int}]
      :kwargs [{:name "save-file" :type :string :default true}]
      :retval :boolean
      }]
```

### call

This Message type actually represents one Module requesting a function call from another Module.

**Request**

| Field       |  Value                | Notes                       |
|-------------|-----------------------|-----------------------------|
| opcode      | 0x02                  |                             |
| source-id   | domain:module-name    |(eg "com.zinger.qe:testMe")  |
| dest-id     | []                    | empty vector                |
| msg-tag     | varies                | integer                     |
| resp-type   | 0x01                  | RESP                        |
| service-tag | name of func          | String                      |
| data        | func-interface        | varies                      |

**Response**


| Field       |  Value                | Notes                       |
|-------------|-----------------------|-----------------------------|
| opcode      | 0x01                  |                             |
| source-id   | controller:path       | eg controller:host-name     |
| dest-id     | []                    | empty vector                |
| msg-tag     | 0x00                  |                             |
| resp-type   | 0x00                  | ACK                         |
| service-tag | nil                   |                             |
| data        | {}                    | function return             |


### send-data

Sends data to another Module.  This is primarily intended for sending large files to another Module.  This will be
the expected Message type for some other commands.  Since the data can be very large, this can be a multi-part
message type.  The first Message will contain the total length of the message.  It is the responsibility of the
receiving Module to know when the data has fully been sent.  In other words, the sending Module does not send a
sentinel value to indicate that the transfer is complete.  If the network goes down for example, the receiving
Module would never get this value anyway.

### get-data

Issues a request to another module for some data, usually a file.

### send-event


### get-event


### subscribe-topic


### unsubscribe-topic


### publish-topic


### unpublish-topic


### list-topic


## Transport Protocol

Here, we will discuss how Messages are actually delivered to Modules.  Because the transport mechanism in pheidippides
varies, how the Message is encoded changes somewhat.  The transport should be bi-directional and reliable.


### TCP/IP Messages

This section describes what Messages are and how a Module will respond to a message.  How a Message will actually be
sent and received will be covered in the Transport Protocol section.

A Message will consist of two parts, a header, and a body.  The header declares the type of the message, and the
total length of the message.  The body contains the actual contents of the message

**Header**

| Field         | Type     | Description
|---------------|:--------:|---------------
| opcode        | Byte     | The type of the message
| length        | Varies   | 1,4, or 8 bytes which contains length of the body. (ignored in some transports)

**Body**

| Field         | Type     | Description
|---------------|:--------:|---------------
| source-id     | String   | A unique string following reverse domain + module name (eg com.zinger.prod:moduleName)
| dest-id       | [String] | A sequence of destination ID's following same type as source-id
| msg-tag       | Integer  | An integer identifier created by the Module unique to that Module
| resp-type     | Byte     | A Byte that determines how to interpret a response
| service-tag   | Varies   |
| data          | Map      | Contains the parameters of a request, the return of a response, or event data

Messages in pheidippides are binary and are encoded and decoded using the ubiquitous MessagePack library.  This has some
consequences for encoding and decoding as described later.

#### Encoding


#### Decoding


### Native Transport

When using a native Transport, messages are sent and received via clojure's core.async channels.