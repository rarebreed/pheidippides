(defproject pheidippides "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [com.taoensso/timbre "4.1.4"]
                 [org.clojure/core.async "0.2.374"]
                 [danlentz/clj-uuid "0.1.6"]
                 [clojure-msgpack "1.1.2"]
                 [prismatic/schema "1.0.5"]
                 [org.msgpack/msgpack "0.6.9"]
                 [org.clojure/core.match "0.3.0-alpha4"]
                 [clj-time "0.11.0"]]

  ;:javac-options ["-target" "1.8" "-source" "1.8"]
  :pom-plugins [[org.apache.maven.plugins/maven-compiler-plugin "3.5.1"
                 {:configuration
                  ([:source "1.8"]
                    [:target "1.8"])}]]
  :java-source-paths ["src/pheidippides/messaging/java"]
  ;:aot [pheidippides.messaging.messages  pheidippides.messaging.controller]
  :main pheidippides.messaging.controller)
