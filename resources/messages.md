# PheiMessages

This document will cover at a high level what the PheiMessage types look like

## Users

One thing that hasn't been really been considered here is the role of users.  For example, some operations on some
services may require escalated permissions.  Currently, the messages here are all assuming a super user type access
but this may not always be the case.

Also we need to consider that some services may be user-agnostic.  For example the Register function just tells the
controlling what is available.

One possibility is to shunt the responsibility for user access control to the services.  Each message could contain
a token or basic auth.  However, that would require each service to authenticate and validate permissions for a user.
A more centralized approach wherein each service tells the controller what user/role it allows when it registers might
be easier to handle logistically, but might hurt performance since all messages have to pass through the controller,
forcing the controller to do auth for every message.  This could be alleviated by a token mechanism such that auth is
done once (for a limited time span)

## TLS

Related to the user concerns is encrypting and decrypting with TLS.  Currently, I just want this as a proof of concept
to get a message bus/rpc mechanism working.  However, for enterprise grade usage, there does need to be consideration
for wrapping and unwrapping messages.  Technically, that lies at one layer above this messaging protocol, but it's still
something to keep in mind.

## Register

Registering a service to the controller provides a way for other other services to see what is available.

Request:
- op-code: 0x01
- ver-major: byte
- ver-minor: byte
- ver-micro: byte
- release-len: short
- name-len: short
- uuid: 16 bytes
- release: String (UTF8)
- name: String


    +------+----------+----------+----------+----------+----------+----------+----------+----------+
    |      |   0      |     1    |     2    |     3    |     4    |     5    |     6    |     7    |
    +------+----------+----------+----------+----------+----------+----------+----------+----------+
    | 0x00 | op-code  | ver-maj  | ver-min  | ver-mic  |    release-len      |       name-len      |
    +------+----------+----------+----------+----------+----------+----------+----------+----------+
    | 0x08 |                                         uuid                                          |
    +------+                                                                                       |
    | 0x10 |                                                                                       |
    +------+----------+----------+----------+----------+----------+----------+----------+----------+
    | 0x18 |                                     release-name                                      |
    |      |                                                                                       |
    |  ?   | 0x18+release-len                                                                      |
    +------+---------------------------------------------------------------------------------------+
    |  ?   | 0x18+release-len+1                      name                                          |
    |      |                                                                                       |
    |  ?   | 0x18+release-len+1+name-len                                                           |
    +------+---------------------------------------------------------------------------------------+

Response:

ack

## Service List

Get a list of all the services connected to the controller.  A flag can indicate whether to get an event when new
services are registered

Request:
- op-code: 0x02
- uuid: 16