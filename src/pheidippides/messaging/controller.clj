(ns pheidippides.messaging.controller
  (:import [java.nio.channels ServerSocketChannel Selector SelectionKey]
           [java.net InetSocketAddress StandardSocketOptions]
           [java.util HashMap])
  (:require [schema.core :as s]
            [taoensso.timbre :as timbre :refer [log info debug spy]]
            [msgpack.core :as msgpack]
            [clojure.core.async :as async :refer [>! <! >!! <!! go go-loop buffer close! thread
                                                  alts! alts!! timeout]]
            [pheidippides.messaging.core :as pmc]
            [pheidippides.messaging.module :as pmod]
            [pheidippides.messaging.messages :as pmm]
            [pheidippides.messaging.log]
            [clojure.core.match :refer [match]]))


(def chan-type :channel-type)
(def server-chan :server-chan)
(def client-chan :client-chan)
(def service-id (atom 0))


(defprotocol PServer
  (select! [this s-chan c-chan] "Implements selection of a ready channel for send/recv of data")
  (handle-client-chan [this chan rc] "handles messages to module")
  (handle-server-chan [this chan rc] "handles messages to server")
  (register-module [this msg] "Registers a Module into Server registry"))


(defn register-module-
  "Takes the :data from msg, and maps it to the :source-id"
  [server msg]
  ())


(defn do-attach
  [server key]
  (let [selector (:selector server)
        ssc (.channel key)
        csc (.accept ssc)]
    (when (not (nil? csc))
      (do
        (.configureBlocking csc false)
        (try
          ;(info "Attaching " server " to " csc " from " selector)
          (let [client-key (.register csc selector SelectionKey/OP_READ SelectionKey/OP_WRITE)
                client-props (HashMap. {chan-type client-chan})
                _ (.attach client-key client-props)]
            (info "Finished attaching " client-props " to " csc))
          (catch Exception ex (str "caught exception: " (.getMessage ex))))))))

(defn server-handler
  "Handler to determine what to do when the selection key is :server-chan

  In this case, we register the client socket channel with the selector for READ and WRITE
  operations.

  *Args*
  - server: Server
  - "
  [server chan r-chan]
  (info "Starting the server-handler")
  (go-loop [key (async/<! chan)]
    (do-attach server key)
    (async/>! r-chan key)
    (recur (async/<! chan))))



(defn do-msg
  [key server]
  (let [client-chan (.channel key)
        [msg op] (pmm/get-chan-msg client-chan)]
    (info msg)
    ;; TODO: based on the opcode, act on the message.
    (match [op]
           [0] (info "TODO: register by taking data from the message, and map source-id to registry.  Send ACK/NAK")
           [1] (info "TODO: unregister by dissoc the source-id from registry, and update database. Send ACK/NAK")
           [2] (info "TODO: look up the registry and send response back to source-id")
           :else (info "TODO: route request message to :destination-id"))))


;; FIXME: The actions should be non-blocking in order to handle a new incoming request.  Turn this into a go-block
;; and the argument will be a channel
(defn client-handler
  "Handler to determine what to do when the selection key is :client-chan.

  In this case, pull the message out of the channel and return the message

  *Args*
  - server: Server
  - key: SelectionKey"
  [server chan r-chan]
  (info "Starting the client handler")
  (go
    (loop [key (async/<! chan)]
      (let [retries (atom 20)]
        (while (and (> @retries 0)
                    (not (.isReadable key)))
          (info "Key is not readable yet...")
          (Thread/sleep 500)
          (swap! retries dec)))
      (do-msg key server)
      (async/>! r-chan key)
      (recur (async/<! chan)))))


(defn work-on-keys
  [chan keys]
  (when (not-empty keys)
    (go
      (doseq [k keys]
        (async/>! chan k)))))


(defn select-
  "Delegation function that takes a Server object and retrieves its Selector object in order to get the
  ready set of SelectedKeys that we are interested in.  Based on how the channel was registered with the
  Selector (and which events of interest), we determine from the Set returned by the Selector what kind
  of channel we got a message from.  This function then delegates to a handler based on the attachment
  from the keys"
  [server s-chan c-chan]
  (let [selector (:selector server)
        sel-keys (.selectedKeys selector)
        key-count (count sel-keys)
        selected-keys (ref {:keyset sel-keys
                            :count key-count})
        seq-keys (seq (:keyset @selected-keys))
        key-types (group-by (fn [k]
                              (let [attach (.attachment k)]
                                (get attach chan-type))) seq-keys)
        client-keys (get key-types client-chan)
        server-keys (get key-types server-chan)]
    (work-on-keys s-chan server-keys)
    (work-on-keys c-chan client-keys)
    sel-keys))


(defrecord RegistryData
  [message-chan
   event-chan
   functions])


(defn register-module-
  "Decodes the msg, and sees if the source-id is already in the registry.

  If it isn't, it will add the following to the registry map:
  - key: source-id
  - value: RegistryInfo
  "
  [server msg mchan echan]
  (let [sid (:source-id msg)]
    (if-not (contains? (server :registry) sid)
      (let [r-info (RegistryData. mchan echan [])]))))

;; ==============================================================================
;; Defines the Server which will hold
(s/defrecord Server
             [host :- s/Str
              port :- s/Int
              block?
              channel :- ServerSocketChannel
              selector :- Selector
              sel-key
              registry
              server-chan
              client-chan
              ]

  PServer
  (select! [this s-chan c-chan]
    (select- this s-chan c-chan))

  (handle-client-chan [this ch rc]
    (client-handler this ch rc))

  (handle-server-chan [this ch rc]
    (server-handler this ch rc)))


(defn attach-chan
  "Attach SelectorKey to a channel type"
  [{:keys [sel-key]}]
  ;; when SelectionKey.attachment() is called, it will get this map
  (.attach sel-key {chan-type server-chan}))


(defn make-server-socket-chan
  "Creates a ServerSocketChannel.
   1. binding it to address and a port.
   2. sets it to non-blocking
   3. Either creates a Selector or uses existing to register Channel with the Selector
   4. Attaches the SelectorKey to a channel type

   returns a map of the Channel, the SelectionKey and the Selector"
  [^String host ^Integer port & {:keys [selector block? registry server-chan client-chan removal-chan]
                                 :or   {block?       false
                                        registry     (atom {})
                                        removal-chan (async/chan 20)
                                        server-chan  (async/chan 20)
                                        client-chan  (async/chan 20)}}]
  (let [ss-chan (ServerSocketChannel/open)
        _ (doto ss-chan
            (.bind (InetSocketAddress. host port))
            (.configureBlocking block?)
            (.setOption  (. StandardSocketOptions SO_REUSEADDR) true))
        selector (if selector
                   selector
                   (Selector/open))
        ;; register the channel to the selector
        selkey (.register ss-chan selector (SelectionKey/OP_ACCEPT))
        _ (info "registered selector:" selector "to channel: " ss-chan)
        sinfo (map->Server {:host         host
                            :port         port
                            :block?       block?
                            :channel      ss-chan
                            :sel-key      selkey
                            :selector     selector
                            :registry     registry
                            :server-chan  server-chan
                            :client-chan  client-chan
                            :removal-chan removal-chan})]
    (attach-chan sinfo)
    (debug "attaching data to SelectionKey")
    sinfo))


(defn serve
  "Starts the ServerSocketChannel to listen for incoming requests

  It uses a Selector object to pick from ready channels "
  [server]
  (let [sc (:server-chan server)
        cc (:client-chan server)
        rc (:removal-chan server)]
    (while true              ;; flag to stop looping
      ;; .select is a blocking method which returns when one of the registered channels is
      ;; selected.  A socket client will be added to the list of registered channels
      (let [selection (.select (:selector server))
            handle-event (fn [s]
                           (swap! service-id #(+ s %))
                           (select! server sc cc))]
        (when (not= selection 0)
          (let [sel-keys (handle-event selection)]
            (loop [kc (count sel-keys)]
              (info (format "There are %d keys left in sel-keys" (count sel-keys )))
              (when-not (= 0 kc)
                (let [key-to-rem (async/<!! rc)]
                  (when (.contains sel-keys key-to-rem)
                    (.remove sel-keys key-to-rem)))
                (recur (count sel-keys))))))))))


(defn -main
  ([host port]
   (let [server (make-server-socket-chan host port)
         ct (handle-client-chan server (:client-chan server) (:removal-chan server))
         st (handle-server-chan server (:server-chan server) (:removal-chan server))
         server-loop (future (serve server))
         client1 (pmod/make->PheiModule2 host port "com.redhat.qe:service1")
         client2 (pmod/make->PheiModule2 host port "com.redhat.qe:service2")

         ;; TODO: Make functions that will create an appropriate PheiMessage type
         msg (pmm/map->PheiMessage {:source-id 1 :dest-id 0 :msg-tag 1 :resp-type 0 :data nil})
         emsg (pmm/encode msg)
         bmsg (pmm/make->UByteBuffer emsg :mode :write)
         hdr (pmm/make-header :register (count emsg))

         ;; Start the ServerSocketChannel to listen for incoming data/connections in its own thread
         prog {:service      server-loop
               :clients      [client1 client2]
               :server       server
               :client-loops [(future (pmod/module-loop client1))
                              (future (pmod/module-loop client2))]
               :header       hdr
               :message      bmsg
               :ct           ct
               :st           st}
         ]
     (Thread/sleep 100)
     (pmod/send-data client1 "com.redhat.qe:service1" {:count 1})
     ;(pmod/register client2)
     ;(>!! (:async-channel client1) hdr)
     ;(>!! (:async-channel client1) bmsg)
     prog))
  ([] (-main "localhost" 13172)))

