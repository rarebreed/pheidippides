(ns pheidippides.messaging.core
  (:import [java.nio.channels Selector SelectionKey SocketChannel]
           [java.nio ByteBuffer]
           [java.nio.charset Charset]
           [java.net InetSocketAddress])
  (:require [schema.core :as s]
            [taoensso.timbre :as timbre :refer [log info debug spy]]
            [clojure.core.async :as async :refer [>! <! >!! <!! go go-loop buffer close! thread
                                                  alts! alts!! timeout]]
            [pheidippides.messaging.messages :as pmm :refer [IMessage UtilMessage]]
            [clojure.core.match :refer [match]]
            [msgpack.core :as msg]))


;; FIXME: This should be a read-channel method for a CharBuffer (from UBuffer protocol)
(defn ^{:deprecated true} get-chan-data
  [chan buff]
  (let [charset (Charset/defaultCharset)]
    (loop [count (.read chan buff)
           msg ""]
      (if (> count 0)
        (do
          (debug count "bytes to read in " chan)
          (.flip buff)
          (recur (.read chan buff) (str msg (.decode charset buff))))
        msg))))


(defn get-at-least
  "Gets at least bytes-to-read bytes from a channel, and stores it into the buffer

  Returns how many bytes in excess of bytes-to-read that is in buff"
  [chan ^ByteBuffer buff bytes-to-read]
  (.flip buff)
  (let [count (.read chan buff)
        remainder (- bytes-to-read count)]
    (if (> remainder 0)
      (recur chan buff remainder)
      remainder)))


(defprotocol ITransport
  "Abstracts out the API for the underlying transport mechanism"
  (connect [this] "Connects the Module transport endpoint to the Controller endpoint")
  (configure [this opts] "Sets up any configuration needed for the endpoint (opts is a map)"))


(defn configure-tcp-ip
  "TODO: make this a multi-method"
  [this opts]
  (match [(opts :type)]
         [:server] (let [chan (:mechanism this)
                         sock-opts (opts :sock-opts)
                         block? (opts :block?)]
                     (doto chan
                       (.bind (InetSocketAddress. ^String (:path this) ^Integer (:endpoint this)))
                       (.configureBlocking block?))
                     (doseq [[opt val] sock-opts]
                       (.setOption chan opt val)))
         [:client] (let [sock-opts (opts :sock-opts)
                         chan (:mechanism this)]
                     (.configureBlocking (:mechanism this) false)
                     (doseq [[opt val] sock-opts]
                       (.setOption chan opt val)))
         :else (throw (ex-info ":type must be :server or :client" {}))))


(defn connect-tcp-ip
  "Function to implement the connect method protocol

  TODO:  Inline this later.  Leaving this as a standalone function makes it easier to debug"
  [this]
  (let [^String path (:path this)
        ^Integer port (:endpoint this)
        chan (:mechanism this)
        sock-addr (InetSocketAddress. path port)
        c? (.connect chan sock-addr)]
    (.isConnected chan)))


;; There are 3 planned transports for pheidippides:
;; - tcp-ip: using non-blocking channels
;; - native: where Modules are local to the Controller and each other
;; - dbus: an adapter that will convert PheiMessages <-> dbus messages
;; Currently, we are only working in the tcp-ip transport, but we must make the code abstract out the underlying
;; transport type
(defrecord TCPIPTransport
  [path                                                     ;; see create-transport
   endpoint                                                 ;; port if using :tcpip
   ^SocketChannel mechanism]                                ;; SocketChannel

  ITransport
  (connect [this]
    (connect-tcp-ip this))
  (configure [this opts]
    (configure-tcp-ip this opts))

  IMessage
  ;; message is a UByteBuffer type
  (pmm/send-message [this message]
    (let [chan (this :mechanism)]
      (pmm/send-message message chan)))
  (pmm/recv-message [this message]
    (let [chan (this :mechanism)]
      (pmm/recv-message message chan))))


;; NativeTransport uses core.async channels (rather than SocketChannel) in order to do the
;; communication.  In this case, the mechanism is a core.async channel, the path is the name
;; of the module (what the Module will register to the controller as, and the endpoint is
;; to be determined
(defrecord NativeTransport
  [path                                                     ;; see create-transport
   endpoint                                                 ;; the module's id
   mechanism]                                               ;; a

  ITransport
  (connect [this])                                          ;; TODO: what is connect to native?
  (configure [this opts])                                   ;; TODO: What is configure to native?

  IMessage
  (pmm/send-message [this message]
    (let [achan (this :mechanism)]
      (async/>!! achan message)))
  (pmm/recv-message [this message]
    (let [achan (this :mechanism)]
      (async/<!! achan))))


(defn create-native-transport
  [path endpoint]
  ;; TODO: I'm still not sure how this is going to work.  My initial thought is I will use core.async
  ;; channels as the mechanism.  The path will be the module id, but not sure if we need an endpoint
  ;; (maybe the name of the interface?)
  (let [mech (async/chan 10)]
    (NativeTransport. path endpoint mech)))


(s/defn make->TCPIPTransport
  "Creates a transport for the PheiModule that is a TCPIPTransport


  "
  [path :- s/Str                                            ;; hostname (or IP addr), module name, or
   endpoint :- s/Int]                                       ;; this is the name of the port
  (let [mech (SocketChannel/open)]
    (TCPIPTransport. path endpoint mech)))


(s/defn make->NativeTransport
  "TODO: make the chan have configurable size"
  [path :- s/Str
   endpoint :- s/Str]
  (let [mech (async/chan 10)]
    (NativeTransport. path endpoint mech)))


(defn transport-factory
  "Creates an implementation of a ITransport and IMessage

    | tran-type | path        | endpoint    |
    |-----------|-------------|-------------|
    | :tcpip    | hostname    | port        |
    | :native   | module name | ?           |
    | :dbus     | bus name    | object name |
  "
  [t-type path endpoint & opts]
  (match [t-type]
         [:tcpip] (make->TCPIPTransport path endpoint)
         [:native] (make->NativeTransport path endpoint)
         :else (throw (ex-info "only :tcpip and :native are supported so far" {}))))


(defn show-selected-keys
  [keyset]
  (let [keys (for [k keyset]
               (str k))]
    (clojure.string/join "," keys)))