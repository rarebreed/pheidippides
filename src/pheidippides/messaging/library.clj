;; Namespace contains a way to describe functions that are exposed in a PheiModule
;; TODO: Create a macro which will extract from all the defn's which have metadata marking
;; them as a ^{:pheimodule true}.  Once all defns which are marked as pheimodules is done,
;; the macro will generate an edn file

(ns pheidippides.messaging.library
  (:require [msgpack.core :as mp]))

(def msgpack-types
  "The allowed datatypes"
  #{:nil
    :boolean
    :integer
    :string
    :float
    :array
    :map
    :extended})

(defrecord Argument
  [^String name                                             ;;
   type])                                                   ;; must be in msgpack-types


(defrecord KWArgument
  [^String name                                             ;; name of the kwarg
   type                                                     ;; its type
   default])                                                ;; default value


;; Information about a Module's function
(defrecord ModuleFunction
  [name                                                     ;; name of the function
   args                                                     ;; a seq of Argument
   kwargs                                                   ;; a seq of KWArgument
   ret-val                                                  ;; type of the return val
   description                                              ;; description of function (can be nil)
   metadata])                                               ;; any metadata of the function (can be nil)


(def example-module-function
  {:name "mymodule"
   :args [(Argument. "age" :integer)]})


(defrecord Library
  [])


(defn add-fn
  "Inserts a function into the library"
  [])
