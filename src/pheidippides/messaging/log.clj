(ns pheidippides.messaging.log
  (:require [taoensso.timbre :as timbre]
            [taoensso.timbre.appenders.core :as appenders]
            [clj-time.core :as ct]
            [clj-time.format :as ctf]))

(defn timbre-mw
  "Reduces the namespace output"
  [data-map]
  (let [ns-info (:?ns-str data-map)
        parts (clojure.string/split ns-info #"\.")
        ns-partial (clojure.string/join "." (map #(first %) (butlast parts)))
        ns-to-use (str ns-partial "." (last parts))]
    (assoc data-map :?ns-str ns-to-use)))


(defn timbre-mw-nohostname
  [data-map]
  (assoc data-map :hostname_ ""))


(defn merge-timbre-mw
  "Installs middlewares (mw) into timbre's config"
  [mw]
  (timbre/merge-config!
    {:middleware mw}))

(merge-timbre-mw [timbre-mw timbre-mw-nohostname])
(timbre/merge-config!
  {:level :info})

(defn home-path
  []
  (let [home (System/getProperty "user.home")
        sep (System/getProperty "file.separator")]
    (str home sep)))

;; FIXME: can't figure out how to get timbre to make the file appender have a log level of :debug or above
(timbre/merge-config!
  {:appenders {:spit (assoc (appenders/spit-appender {:fname (str (home-path) "pheidippides.log")})
                       :level :debug )}})