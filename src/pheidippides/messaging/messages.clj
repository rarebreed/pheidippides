(ns pheidippides.messaging.messages
  (:require [msgpack.core :as msg]
            [msgpack.clojure-extensions]
            [msgpack.macros :refer [extend-msgpack]]
            [schema.core :as s]
            [taoensso.timbre :as timbre]
            [clojure.core.async :as async])
  (:import [java.nio ByteBuffer]
           [java.nio.channels SocketChannel]
           [clojure.lang Keyword]))

(def msg-map {:register {:opcode 0 :size 4}
              :unregister {:opcode 1 :size 1}
              :interface-list {:opcode 2 :size 2}
              :call {:opcode 3 :size 4}
              :send-data {:opcode 4 :size 8}
              :get-data {:opcode 5 :size 8}
              :send-event {:opcode 6 :size 8}
              :get-event {:opcode 7 :size 8}
              :subscribe-topic {:opcode 8 :size 4}
              :unsubscribe-topic {:opcode 9 :size 2}
              :publish-topic {:opcode 0x0a :size 4}
              :unpublish-topic {:opcode 0x0b :size 2}
              :list-topic {:opcode 0x0c :size 2}})

(def op-map
  "Creates a mapping from opcode to {:service message-type :size length-in-bytes}"
  (into {} (for [[k v] msg-map]
             (let [key (:opcode v) val {:service k :size (:size v)}]
               [key val]))))


(defprotocol UtilMessage
  (encode [this] "Puts data in a form to be Serialized on the wire (a byte array)")
  (decode [this] "deserializes data on the wire into a data structure"))


(defprotocol IMessage
  "Encapsulates how to read and write from an underlying Transport type

  Since pheidippides can use one of several transports, this protocol defines how to
  read and write messages to that transport.  For example, TCP/IP transports will
  define how to read and write from a SocketChannel

  Args
  - this:  The defrecord object
  - other: could be a transport record, or a message record depending on the this type"
  (recv-message [this other])
  (send-message [this other]))


(s/defrecord PheiHeader
  [opcode :- s/Int                                          ;; The type in TLV
   length :- s/Int                                          ;; The length in TLV
   ])


(defn -encode [this]
  (msg/pack this))

(defn -decode [this]
  (msg/unpack this))


(s/defrecord PheiMessage
  [source-id :- Integer      ;; id is the uuid of the service(akin to IP port)
   dest-id :- [Integer]      ;; The id of the destination service or Topic
   msg-tag :- s/Int          ;; message ID
   resp-type :- Byte         ;; 0x00 = ack
                             ;; 0x01 = Service response
                             ;; 0x02 = multipart data
                             ;; 0x03 = streaming data
   service-tag :- Byte       ;; when resp-type = 0x00, nil
                             ;;                  0x01, the name of service function
                             ;;                  0x02 or 0x03, nil when done, 1 otherwise
                             ;; if streaming data: it will be
   data                      ;; msgpack data (params or result)
   ]

   UtilMessage
   (decode [this]
     (-decode this))

   (encode [this]
     (-encode this)))


(defn- -read-channel
  "Defines how to get a message from a transport that is a SocketChannel"
  [this chan]
  (let [mode   @(:mode this)
        buffer (:buffer this)
        desc "before pos: %d, before remaining: %d, after pos: %d, after remaining: %d, bytes read: %d"]
    (timbre/debug "in -read-channel: this is " this " channel is " chan " buff limit is " (.limit buffer))
    (when (= mode :write)
      (.flip buffer)
      (reset! (:mode this) :read))
    (while (.hasRemaining buffer)
      (timbre/debug "##### In while loop, -read-channel " buffer)
      (let [before-pos (.position buffer)
            before-remaining (.remaining buffer)
            bytes-read (.read chan buffer)
            after-pos (.position buffer)
            after-remaining (.remaining buffer)]
        (timbre/debug (format desc before-pos before-remaining after-pos after-remaining bytes-read))))
    (timbre/info "Finished reading from " chan " into " buffer)
    (.flip buffer)
    buffer))


(defn- -write-channel
  "Defines how to write a message to a SocketChannel transport"
  [this chan]
  (timbre/debug "in -write-channel: this is " this " channel is " chan)
  (let [mode @(:mode this)
        buffer (:buffer this)]
    (when (= mode :read)
      (reset! (:mode this) :write))
    (while (.hasRemaining buffer)
      (.write chan buffer))
    (timbre/info "In -write-channel: finished writing buffer " buffer " to chan " chan)
    buffer))


(defrecord UByteBuffer
  [^Integer size          ;; how many bytes to allocate
   buffer                 ;; A byte buffer of some sort.
   mode]

  UtilMessage
  (decode [this]
    (msg/unpack (.array (:buffer this))))

  (encode [this]
    (.array (:buffer this)))

  IMessage
  (recv-message [this chan]
    (-read-channel this chan))

  (send-message [this chan]
    (-write-channel this chan)))


(defn make->UByteBuffer
  "Creates a UByteBuffer

   msg can be a size or a byte array
   mode can be keyword specifying either :read or :write"
  [msg & {:keys [mode]
      :or {mode :read}}]
  (let [ubb {:buffer (if (isa? (type msg) Number)
                       (ByteBuffer/allocate msg)
                       (ByteBuffer/wrap msg))
             :size   (if (isa? (type msg) Number)
                       msg
                       (count msg))
             :mode   (atom mode)}]
    (map->UByteBuffer ubb)))


(defn get-bytes-to-read
  "Creates a buffer of one byte (enough to get the opcode), and reads from the chan into
  this buffer.

  Returns a map of the :opcode byte, and :bytes-to-read which is how many more bytes to read"
  [chan]
  (let [buff (ByteBuffer/allocate 1)
        desc "before pos: %d, before remaining: %d, after pos: %d, after remaining: %d, bytes read: %d"
        _ (while (.hasRemaining buff)
            (timbre/debug "##### In get-bytes-to-read while loop " buff)
            (let [before-pos (.position buff)
                  before-remaining (.remaining buff)
                  bytes-read (.read chan buff)
                  after-pos (.position buff)
                  after-remaining (.remaining buff)]
              (timbre/info (format desc before-pos before-remaining after-pos after-remaining bytes-read))))
        _ (.flip buff)
        opcode (.get buff)
        bytes-to-read (->> opcode (get op-map) :size)]
    (cond
      (nil? bytes-to-read) (throw (Exception. "Can't have bytes-to-read be nil"))
      (= 0 bytes-to-read) (throw (Exception. "Can't have bytes-to-read be 0"))
      :else (timbre/debug "bytes-to-read is " bytes-to-read))
    {:opcode opcode
     :bytes-to-read bytes-to-read}))


(defn get-msg-len
  "Takes the header from the byte stream (the TL in TLV) in order to get the
  message type and length."
  [^SocketChannel chan & {:keys [opcode]}]
  (let [{:keys [bytes-to-read opcode]} (if (nil? opcode)
                                         (get-bytes-to-read chan)
                                         {:opcode opcode :bytes-to-read (:size (op-map opcode))})
        ^UByteBuffer buff (make->UByteBuffer bytes-to-read)
        buff (recv-message buff chan)
        msg-size (cond
                   (= 1 bytes-to-read) (.get buff)
                   (= 4 bytes-to-read) (.getInt buff)
                   :else (.getLong buff))
        msg-size (if (< msg-size 0)
                   (BigDecimal. (Long/toUnsignedString msg-size))
                   msg-size)]
    {:msg-size msg-size :opcode opcode}))

;; TODO: Turn get-chan-msg into a method in a protocol.  The TCPIPTransport and
(defn get-chan-msg
  "Pulls data from the channel, determining the type of message and the length of data.
   extracting more data from the channel
  until the messge is fully extracted

  Returns:
  A vector of the map of the message and the opcode of the message"
  [!chan]
  (let [{:keys [msg-size opcode]} (get-msg-len !chan)
        buff (make->UByteBuffer msg-size)]
    (recv-message buff !chan)
    [(decode buff) opcode]))


(defn make-header
  "Creates a ByteBuffer of the appropriate type for each opcode and returns it

  This is the first thing the service will send, followed by the message itself"
  [^Keyword msg-type msg-size]
  (let [mm (msg-type msg-map)
        {:keys [opcode size]} mm
        hdr (make->UByteBuffer (+ 1 size))
        bb (:buffer hdr)]
    ;; mutating the ByteBuffer
    (.put bb (byte opcode))
    (cond
      (= size 1) (.put bb (byte msg-size))
      (= size 2) (.putShort bb (short msg-size))
      (= size 4) (.putInt bb (int msg-size))
      :else (.putLong bb (long msg-size)))
    (.flip bb)
    hdr))


(defn make-msg-buffer
  "Given the header and message, create a single ByteBuffer that contains both"
  [^Keyword msg-type ^bytes msg]
  (let [{:keys [opcode size]} (msg-type msg-map)
        msg-size (count msg)
        total-size (+ 1 size msg-size)
        buff (make->UByteBuffer total-size)
        bb (:buffer buff)]
    ;; mutating the ByteBuffer
    (.put bb (byte opcode))
    (cond
      (= size 1) (.put bb (byte msg-size))
      (= size 2) (.putShort bb (short msg-size))
      (= size 4) (.putInt bb (int msg-size))
      :else (.putLong bb (long msg-size)))
    (.put bb msg)
    (.flip bb)
    buff))


;; There's a subtype of ArgType that has an extra field called default
(defrecord ArgType
  [name type])


(s/defrecord func-interface
  [name :- s/Str                                            ;; function name
   arg-types :- [ArgType]                                   ;; positional args
   kwarg-types :- [ArgType]])                               ;; keyword args


(defn service-send-msg
  [service]
  (let [achan (:async-channel service)]))