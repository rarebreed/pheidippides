(ns pheidippides.messaging.module
  (:import [java.nio ByteBuffer]
           [pheidippides.messaging.messages PheiMessage])
  (:require [clojure.core.async :as async :refer [>! <! >!! <!! go go-loop buffer close! thread
                                                  alts! alts!! timeout]]
            [clojure.core.match :refer [match]]
            [taoensso.timbre :as timbre :refer [log info debug spy]]
            [msgpack.core :as mp]
            [msgpack.clojure-extensions]
            [msgpack.macros]
            [pheidippides.messaging.messages :as pmm :refer [get-chan-msg]]
            [pheidippides.messaging.core :as pcore]
            [pheidippides.messaging.library :as plib]
            [schema.core :as s]))


(def arg-data {})

(defprotocol MessageService
  (register [this] "Register with the server and provide the ID of this client")
  (service-list [this] "Shows other client services")
  (service-request [this msg] "Calls a function on a service and gets response back")
  (service-query [this msg] "Get the API of a service")
  (init-data-send [this msg] "Sends data to service")
  (init-data-get [this msg] "Retrieves data from a service")
  (send-event [this msg] "Sends an event type to a topic")
  (subscribe [this msg] "Subscribes to get events/data from a topic")
  (publish [this msg] "Creates a topic for other services"))


(defn wire-setup
  [svc-type & {:keys [msg]}]
  (let [pm (if (= (type msg) PheiMessage)
             msg
             (pmm/map->PheiMessage msg))
        wire (pmm/encode pm)
        msg (pmm/make->UByteBuffer wire :mode :write)
        header (pmm/make-header svc-type (count wire))]
    [msg header]))


(defn setup-wire
  [svc-type & {:keys [msg]}]
  (let [pm (if (= (type msg) PheiMessage)
             msg
             (pmm/map->PheiMessage msg))
        wire (pmm/encode pm)
        msg (pmm/make-msg-buffer svc-type wire )]
    msg))

;; Although this hurts performance, leave this as a separate fn instead of defined
;; in PheiModule to make debugging easier
(defn register-module
  "Registers a Module with the Controller.

  The message's data should contain a map"
  [module]
  (let [m {:source-id      (:id module)
           :dest-id        0
           :service-tag    nil
           :resp-type      0
           :data           nil}
        pm (setup-wire :register :msg m)
        achan (:async-channel module)]
    (go
      (>! achan pm))))


(defn send-data
  [module dest-id data]
  (let [m {:source-id      (:id module)
           :dest-id        dest-id
           :service-tag    nil
           :resp-type      0
           :data           data}
        pm (setup-wire :send-data :msg m)
        achan (:async-channel module)]
    (go
      (>! achan pm))))


;; The PheiModule is the generator of PheiMessages.  It will embed in the messages it sends it's address, port,
;; and ID.  The id is an identifier of what kind of client it is so that the clients it sends messages to
;; will know what kind of client it is dealing with
(defrecord PheiModule
  [^String path            ;; the IP address or hostname to connect to
   endpoint                ;; the port to connect on remote host
   ^String id              ;; a name for this client
   transport               ;; a Transport object for regular messages
   trans-evt               ;; A Transport object event messages
   library                 ;; A Library of the Module's service functions
   async-channel]          ;; A core.async channel (default- created by make-client)

  MessageService
  (register [this]
    (register-module this))

  (service-list [this]
    (let [m {:source-id (:id this) :dest-id 0 :service-tag "service-list" :resp-type 1 :data nil}
          [pm wire hdr] (wire-setup :service-list :msg m)
          achan (:async-channel this)]
      {:message pm
       :sent? (go
                (>! achan hdr)
                (>! achan wire))}))

  (service-request [this msg]
    (let [wire (pmm/encode msg)
          ])))


;; TODO: This will only work for TCPIPTransport
;; TODO: The client is only listening to the regular transport, not trans-evt
(defn module-loop
  [client & {:keys [buff]
             :or {buff (ByteBuffer/allocate 256)}}]
  (info "In module-loop for " (:id client))
  (let [chan (-> (:transport client) :mechanism)
        achan (:async-channel client)]
    (while (not (.finishConnect chan))
      (timbre/warn "Waiting to connect..."))
    (info (:id client) "has finished connecting")
    ;; Read data from async channel.  This will spawn some new threads in a thread pool
    ;; and if there's no data in achan, it will park.  This code effectively runs in a new pool
    ;; TODO: data will be PheiMessage
    (go-loop [data (<! achan)]
      (try
        (timbre/warn (format "In %s: going to send data: %s" (:id client) (str data)))
        (pmm/send-message data chan)
        (catch Exception ex (str (.getMessage ex))))
      (recur (<! achan)))
    ;; FIXME We need a way for the module to get messages back from the Controller
    ))

;; FIXME: This part of the code is broken.  Might move this part into a Selector
(comment
  (loop [[chan-msg opcode] (get-chan-msg chan)]
    ;; TODO: Add logic here to do something with the request
    (info "TODO:  Make the client service the message")
    (recur (get-chan-msg chan))))


(defn make->PheiModule2
  [^String path endpoint ^String mod-name & {:keys [t-type t-msg t-evt]
                                             :or {t-type :tcpip}}]
  (let [trans-msg (if t-msg t-msg (pcore/transport-factory t-type path endpoint))
        trans-evt (if t-evt
                    t-evt
                    (match [t-type]
                           [:tcpip] (pcore/transport-factory t-type path (inc (Integer. endpoint)))
                           [:native] (pcore/transport-factory t-type path endpoint)))
        achan (async/chan 10)]
    (doseq [trans [trans-msg trans-evt]]
      (pcore/configure trans {:type :client})
      (pcore/connect trans))
    (map->PheiModule {:path path :endpoint endpoint :transport trans-msg :trans-evt trans-evt
                      :async-channel achan :id mod-name})))
