(ns pheidippides.core-test
  (:import [java.nio ByteBuffer])
  (:require [clojure.test :refer :all]
            [pheidippides.core :refer :all]
            [pheidippides.messaging.module :as pmod]
            [pheidippides.messaging.messages :as pmm]
            [pheidippides.messaging.controller :as pmc]
            [pheidippides.messaging.core :as pcore]))


(def svc-id "pheidippides.rarebreed.com.github")


(defn create-chan-buffer
  []
  (let [client (pms/make->PheiModule "localhost" 13172 svc-id)
        chan (:channel client)
        wb (ByteBuffer/allocate 32)
        rb (ByteBuffer/allocate 32)]
    [client chan wb rb]))


(deftest a-test
  (testing "Getting data from ByteBuffer"
    (let [[svc chan wb rb] (pmc/make-server-socket-chan "localhost" 13172)
          ])
    (is (= 0 1))))
